﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleBehaviour : StateMachineBehaviour
{
   [SerializeField]
    private float _timeUntilFinish;

    [SerializeField]
    private int _numberOfAnimations;

    private bool _isIdle;
    private float _idleTime; 
    private int _idle_type; 

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        ResetIdle();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_isIdle == false)
        {
            _idleTime += Time.deltaTime;

            if (_idleTime > _timeUntilFinish && stateInfo.normalizedTime % 1 < 0.02f) 
            {
                _isIdle = true;
                _idle_type = Random.Range(1, _numberOfAnimations + 1);
                _idle_type = _idle_type * 2 - 1;

                animator.SetFloat("idle_type", _idle_type - 1);
            }
        }
        else if (stateInfo.normalizedTime % 1 > 0.98)
        {
            ResetIdle();
        }

        animator.SetFloat("idle_type", _idle_type, 0.2f, Time.deltaTime);
    }

    private void ResetIdle()
    {
        if (_isIdle)
        {
            _idle_type--;
        }

        _isIdle = false;
        _idleTime = 0;
    }

}
