﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]

public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;

    public float rotationThreshold = 0.3f;

    public int degreesToTurn = 160;

    [Header("Animator Parameters")]

    public string motionParam = "motion";
    public string mirrorIdleParam ="mirrorIdle";
    public string turn180Param = "turn180";
    public string IsJumpingParam = "IsJumping";
    public string IsGroundedParam = "IsGrounded";
    public string CrouchParam="Crouch";
     

   

    public Rigidbody rb;
    public float Jumpforce=8f;
    public bool puedosaltar;
    public bool atacking;
    public bool avance;
    public float punchimpulse;



    

    [Header("Animation Smoothing")]
    [Range(0, 1f)]

    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private float Speed;
    private float Idle;

    private Vector3 desiredMoveDirection;
    private CharacterController charactercontroller;
    private Animator animator;


    private bool mirrorIdle;

    private bool turn180;

    private bool IsJumping;
    private bool IsGrounded;
    

    
 



    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody>();
        charactercontroller=GetComponent<CharacterController>();
        puedosaltar=false;
        animator=GetComponent<Animator>();
        Cursor.visible=false;
        Cursor.lockState=CursorLockMode.Locked;
        
    }

    

    public void moveCharacter(float hInput,float vInput,Camera cam,bool jump,bool dash){

        //Calculate Input Magnitude
        jump=false;
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        if(Speed >= Speed-rotationThreshold && dash){
            Speed = 1.5f;
             }

        
        if(Speed > rotationThreshold){

            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y= 0f;
            right.y =0f;

            forward.Normalize();
            right.Normalize();

            desiredMoveDirection = forward * vInput + right * hInput;

            if(Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn){

                turn180 = true;
                 }else{

                    turn180 = false;
                    transform.rotation = Quaternion.Slerp(transform.rotation,
                                                    Quaternion.LookRotation(desiredMoveDirection),
                                                    rotationSpeed * Time.deltaTime);

                    }
            
            animator.SetBool(turn180Param, turn180);
            }

        else if(Speed < rotationThreshold){

            animator.SetBool(mirrorIdleParam, mirrorIdle);
            //Stop the character
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
             }

             //JumpAnimation
        if(puedosaltar==true || IsGrounded && !atacking){

            if(Input.GetKey(KeyCode.Space)){

                animator.SetBool("IsJumping", true);
                rb.velocity = new Vector3(0, Jumpforce, 0);
                animator.SetBool("IsGrounded", false);
                
                
                puedosaltar=false;
                
                }

            animator.SetBool("IsGrounded",true);
          
            }
            else{
                Falling();
                animator.SetBool("IsGrounded",false);
            }

            //Dances
            if(Input.GetKey("1")){
                animator.SetBool("Other",false);
                animator.Play("Arms Hip Hop Dance");

                }

            if(Speed>0.1 || 0.1>Speed || puedosaltar==true){
                 animator.SetBool("Other",true);
                 }

            if(Input.GetKey("2")){
                animator.SetBool("Other",false);
                animator.Play("Macarena Dance");

                }

            if(Speed>0.1 || 0.1>Speed || puedosaltar==true){
                 animator.SetBool("Other",true);
                 }

            if(Input.GetKey("3")){
                animator.SetBool("Other",false);
                animator.Play("Praying");

                }

            if(Speed>0.1 || 0.1>Speed || puedosaltar==true){
                 animator.SetBool("Other",true);
                 }


            //Atack
            if(Input.GetKeyDown(KeyCode.Return) && puedosaltar && !atacking){

                animator.SetTrigger("Atackpunch");

                }

            if(Input.GetKey(KeyCode.C) && puedosaltar && !atacking){

                animator.SetFloat(CrouchParam,Speed,StartAnimTime,Time.deltaTime);

                if(Input.GetKey(KeyCode.Q)){

                    return;
                     }



                 }
        }
        
    public void Exitpunch(){
            atacking=false;
        

        }

    public void avanzosolo(){
           
            avance=true;

        }

    public void dejodeavanzar(){
           
            avance=false;

        }

    public void Falling(){

       animator.SetBool("IsGrounded", false);
       animator.SetBool("IsJumping", false);
       

        }

    private void OnAnimatorIK(int layerIndex) {

        if(Speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position,animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position,animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if(distanceToRightFoot > distanceToLeftFoot){

            mirrorIdle=true;

            }else{

                mirrorIdle=false;
                 }

        }

        }

        

    

    
    
    

